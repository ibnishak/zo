source ${ZOCONFIG:-~/.config/zo/settings.zsh}
today(){
  cd ${WIKIROOT}/$(date +%Y)/$(date +%m)/$(date +%d)
  ls
}
zolaunchwidget(){
  zo $(rg -oI -g "*.zsh" "\s*\#\?\s*(.*?)[\[|:]" -r '$1' /data/data/com.termux/files/home/.local/share/zo/zo-lib |fzf)
}
zle -N zolaunchwidget zolaunchwidget

zolaunchsnippetwidget(){
      zo launch snippet
}
zle -N zolaunchsnippetwidget zolaunchsnippetwidget


alias zal="zo add log"
alias zap="zo add page"
alias zaq="zo add quicknote"
alias zas="zo add snippet"
alias zau="zo add url"
alias zcl="zo copy link"
alias zct="zo copy tags"
alias zls="zo launch snippet"
alias zof="zo open folder"
alias zoi="zo open index"
alias zop="zo open page"
alias zos="zo open settings"
alias zos="zo open snippet"
alias zs="zo search"

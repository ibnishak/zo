# WIP

## What is zo
Zo is my collection of **zsh** scripts and few golang programs used to build my personal knowledge base.

## Dependencies
- fzf
- fd
- rg
- ambr
- xclip
- sed
  
## Addons
- Scraper - Go program
- Replacer - Require [Amber](https://github.com/dalance/amber)

## How to install
1. Clone this repo
2. Run **zo-install.sh**

## What are the commands available
Just type `zo` and see the help menu
  
## Other Links of interest
- [Markdown All in One Plugin for VS Code](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
- [VS Code Extension to create markdown TOC](https://marketplace.visualstudio.com/items?itemName=AlanWalk.markdown-toc)
- [Markdown lint plugin for VS Code](https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint)
- [Markdown interpreter in bash](https://github.com/chadbraunduin/markdown.bash)
- [Shelldoc: Execute cmds written in md and compare it with results written in md](https://github.com/endocode/shelldoc)
- [Another bash script compiler and interpreter for markdown files](https://github.com/bashup/mdsh)
- [Rust crate to view markdown files directly in terminal](https://crates.io/crates/mdcat)
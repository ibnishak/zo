#!/data/data/com.termux/files/usr/bin/env zsh
source ./lib/helpers.zsh
DEST="${HOME}/.local/share/zo"
CONF="${HOME}/.config/zo"
BIN=""
function intro {
    autoload colors && colors
    _line red
    _center "Zo" "=" 6
    _line red
}
function dependecy_check {
    _grey "\n\nChecking dependencies..."
    command -v fzf >/dev/null 2>&1 \
        || { echo >&2 "fzf not found. Aborting"; exit 1; }
    command -v rg >/dev/null 2>&1 \
        || { echo >&2 "rg not found. Aborting"; exit 1; }
    _color 220
    printf "\nDependency check: \e[7m SUCCESS \e[27m\n\n"
    _coloroff
}

function destpick {
    printf "\nSelect where to install zo.\n"
    _yellow "Ensure you have privileges to install to the selected path\n\n"
    local destpaths=($(echo $PATH | tr ":" "\n"))
    select destpath in $destpaths; do
        BIN=$destpath
        break
    done
    _green "\nSelected path: $BIN\n\n"
}

function copytodest {
    mkdir -p $DEST
    cp zo ${DEST}
    cp -r lib ${DEST}
    ln -sfni ${DEST}/zo $BIN/zo
    if [ -d ${CONF} ]; then
        _yellow "Existing configuration found"
        _red "KEEP existing configuration? (Y/n): "
        read -sk bak
        if [ "$bak" = "n" ]; then
            rm -rf ${CONF}
            cp -r config ${CONF}
            _yellow "Settings overwritten"
        else
            _green "Existing settings KEPT"
        fi
    else
        cp -r config ${CONF}
    fi
}

function addonpicker() (
    _red "Install addons? (y/n): "
    read -sk add
    if [ "$add" = "y" ]; then
        declare -a addons
        addons=($(fd  --type f -e zsh . ./addons | fzf --phony -m --prompt "Select addons: "))
        for addon in ${addons}; do
            fn=$(echo $(basename ${addon}) | sed 's/.*--//')
            cp ${addon} ${DEST}/lib/$fn
        done
    fi
)

function termuxpatch()(
    if [[ "$OSTYPE" == "linux-android"* ]]; then
        termux-fix-shebang ${DEST}/zo
        _green "Patching for termux DONE!\n"
        mkdir -p "${HOME}/.shortcuts"
        local zcl=$(rg -oI -g "*.zsh" "\s*\#\?\s*(.*?)[\[|:]" -r '$1' ./lib | sed "s/\[.*\]//g" |  sort)
        local zocmds=("${(f)zcl}")
        for zocmd in ${zocmds}; do
            echo "${BIN}/zo ${zocmd}" > ${HOME}/.shortcuts/zo-$zocmd
        done
        _green "Copied Termux shortcuts for Zo\n"
    fi
)

intro
dependecy_check
destpick
copytodest
addonpicker
chmod +x ${DEST}/zo
termuxpatch
_green "Installation complete\n"
_green "OPTIONAL: You may add the following line to zshrc for aliases and widgets\n"
echo "source ${(D)CONF}/zo-aliases.zsh"

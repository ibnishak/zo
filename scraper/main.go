package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
	"text/template"
	"time"

	readability "github.com/go-shiori/go-readability"
	"github.com/kennygrant/sanitize"
	"github.com/mattn/godown"
)

// Org struct
type Org struct {
	Title   string
	Author  string
	Length  int
	Source  string
	Content string
	Created string
}

var (
	a = flag.String("add", "", "Url to be parsed")
)

func main() {
	flag.Parse()

	article, err := readability.FromURL(*a, 30*time.Second)
	if err != nil {
		fmt.Println("Failed to parse the url with error:", err)
	}
	b := sanitize.Name(fmt.Sprintf("%v.md", article.Title))
	if err != nil {
		fmt.Println("Error in sanitizing article title: ", err)
	}
	file, err := os.Create(b)
	if err != nil {
		fmt.Println("Error in file creation: ", err)
		return
	}
	defer file.Close()

	var buf bytes.Buffer
	err = godown.Convert(&buf, strings.NewReader(article.Content), new(godown.Option))
	if err != nil {
		fmt.Println("Error in converting to md")
	}
	regex, err := regexp.Compile("\n\n\n")
	if err != nil {
		log.Println("", err.Error())
	}
	s := regex.ReplaceAllString(buf.String(), "\n")
	o := Org{article.Title, article.Byline, article.Length, *a, s, fmt.Sprintln(time.Now().Format("2006-01-02--15-04-05"))}

	tmpl := template.New("org")

	//parse some content and generate a template
	tmpl, err = tmpl.Parse("- #+TITLE: {{.Title}}\n- #+AUTHOR: {{.Author}}\n- #+LENGTH: {{.Length}}\n- #+SOURCE: {{.Source}}\n- #+CREATED: {{.Created}}\n---\n\n{{.Content}}")
	if err != nil {
		fmt.Println("Error in template parsing: ", err)
		return
	}
	err1 := tmpl.Execute(file, o)
	if err1 != nil {
		fmt.Println("Error in template execution: ", err1)
		return
	}
}

module gitlab.com/ibnishak/zo/zo-scraper

go 1.13

require (
	github.com/go-shiori/go-readability v0.0.0-20191003101053-c43f2838d3f5
	github.com/kennygrant/sanitize v1.2.4
	github.com/mattn/go-runewidth v0.0.4 // indirect
	github.com/mattn/godown v0.0.0-20180312012330-2e9e17e0ea51
)

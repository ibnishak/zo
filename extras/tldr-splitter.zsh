#!/usr/bin/env zsh

setopt nonomatch
file="$1"
folder=$(basename $file .md)
mkdir $folder
csplit -z "$1" /^-/ {*}
rm -f xx00
for i in xx*; do
slug=$(cat $i \
        | head -n 1 \
        | sed "s/[^A-Za-z ]//g" \
        | sed "s/^\s*//").md
cat << eof > $slug
\`\`\`bash
$(cat $i | sed "s/\`//g")
\`\`\`
eof
sed -i '/^-/d' "$slug"
sed -i -r "s/\{\{(.[^}]*)\}\}/<<\1>>/g" "$slug"
sed -i "/^\s*\t*$/d" "$slug"
rm -f $i
mv "$slug" "$folder"
done

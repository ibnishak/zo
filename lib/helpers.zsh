#===================COLORS=============================#

# Usage
# Multiline outputs like heredoc can be colored as the following
# color 123
# <code>
# coloroff
# Single line outputs are colored as _red "Hellooo"
function _color {
    printf "\e[38;5;${1}m"
}

function _coloroff {
    printf "\e[0m"
}

function _color_inline {
    printf "\n\e[%dm%s\e[0m" "$1" "$@"
}

function _red {
    printf "\n\e[31m$@\e[0m"
}
function _green {
    printf "\n\e[32m$@\e[0m"
}
function _yellow {
    printf "\n\e[33m$@\e[0m"
}
function _blue {
    printf "\n\e[34m$@\e[0m"
}
function _magenta {
    printf "\n\e[35m$@\e[0m"
}
function _cyan {
    printf "\n\e[36m$@\e[0m"
}

function _grey {
    printf "\n\e[90m$@\e[0m"
}

# Dracula color scheme for fzf
export FZF_DEFAULT_OPTS=$FZF_DEFAULT_OPTS'
--color=dark
--color=fg:-1,bg:-1,hl:#5fff87,fg+:-1,bg+:-1,hl+:#ffaf5f
--color=info:#af87ff,prompt:#5fff87,pointer:#ff87d7,marker:#ff87d7,spinner:#ff87d7
'

### Center Align text
if (($COLUMNS > 85));then
    cols=85
else
    cols=$COLUMNS
fi


_center()
{
# first argument: text to center
# second argument: glyph which forms the border
# third argument: width of the padding
# eg: center "Something I want to print" "=" 6
    local terminal_width=$cols
    local text="${1:?}"
    local glyph="${2:-=}"
    local padding="${3:-2}"
    local text_width=${#text}
    local border_width=$(( (terminal_width - (padding * 2) - text_width) / 2 ))

    local border=
    for ((i=0; i<border_width; i++))
    do
        border+="${glyph}"
    done

    # a side of the border may be longer (e.g. the right border)
    if (( ( terminal_width - ( padding * 2 ) - text_width ) % 2 == 0 ))
    then
        # the left and right borders have the same width
        local left_border=$border
        local right_border=$left_border
    else
        # the right border has one more character than the left border
        # the text is aligned leftmost
        local left_border=$border
        local right_border="${border}${glyph}"
    fi

    # space between the text and borders
    local spacing=

    for ((i=0; i<$padding; i++))
    do
        spacing+=" "
    done

    printf "\n${left_border}${spacing}${text}${spacing}${right_border}\n"
}

_line(){
    printf $fg[$1]${(l:$cols::=:)}$reset_color
}

initwiki(){
    echo "Unable to find ${WIKIROOT}."
    echo "Creating the directory anew"
    mkdir -p ${WIKIROOT}
    mkdir -p ${TAGS}
    mkdir -p ${INBOX}
    mkdir -p ${SNIPPETS}
}

function makealiases(){
    local zcl=$(rg -oI -g "*.zsh" "\s*\#\?\s*(.*?)[\[|:]" -r '$1' ./lib | sed "s/\[.*\]//g" |  sort)
    local zocmds=("${(f)zcl}") #Need global so termuxpatch can use it
    cat<<-eof >> "./config/zo-aliases.zsh"
	source \${ZOCONFIG:-~/.config/zo/settings.zsh}
	today(){
	  cd \${WIKIROOT}/\$(date +%Y)/\$(date +%m)/\$(date +%d)
	  ls
	}
	zolaunchwidget(){
	  zo \$(rg -oI -g "*.zsh" "\s*\#\?\s*(.*?)[\[|:]" -r '\$1' ${DEST}/lib |fzf)
	}
	zle -N zolaunchwidget zolaunchwidget

	zolaunchsnippetwidget(){
      zo launch snippet
	}
	zle -N zolaunchsnippetwidget zolaunchsnippetwidget
	
	eof
    for zocmd in ${zocmds}; do
        words=(${(ps: :)${zocmd}})
        zocmd=$(echo $zocmd | sed "s/\s*$//")
        printf "\nalias z%s=\"zo $zocmd\"" "${words[1]:0:1}${words[2]:0:1}" >> ./config/zo-aliases.zsh
    done
}

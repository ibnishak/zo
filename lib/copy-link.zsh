function copylink {
    #? copy link :: Create and Copy links to clipboard
    LINKTO="$@"
    if [ -z "$LINKTO" ]; then
        vared -p "Enter the desired title to link to:" -c LINKTO
    fi
    if [ -z "$LINKTO" ]; then
        echo "No search term provided. zo Exiting"
        exit 2
    fi
    LINK=$(fd --type f --extension md ${LINKTO} | ${LAUNCHER})
    if [ -z "$LINK" ]; then
        echo "No results found. zo Exiting"
        exit 3
    fi
    printf "[](../../../%s)" "${LINK}" | ${CLIP}
    printf "$fg[green]Copied link!\n"
}

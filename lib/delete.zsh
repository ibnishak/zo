function deletefile {
    #? delete file :: Deltes file, removes entry from tag files and shows list of files with link to file
    LINKTO="$@"
    if [ -z "$LINKTO" ]; then
        vared -p "Enter the desired title to link to:" -c LINKTO
    fi
    if [ -z "$LINKTO" ]; then
        echo "No search term provided. zo Exiting"
        exit 2
    fi
    LINK=$(fd --type f --extension md ${LINKTO} | ${LAUNCHER})
    if [ -z "$LINK" ]; then
        echo "No results found. zo Exiting"
        exit 3
    fi
    rm ${LINK}
    printf "${LINK} has been deleted."
    rg -l ${LINK} tags | while read -r tagfile ; do
       rg -v ${LINK} ${tagfile} > ${tagfile}.bak && mv ${tagfile}.bak ${tagfile}
    done
    local linkfrom=$(rg -l ${LINK} 20*)
    if [ -z "$linkfrom" ]; then
        _green "There are no files linking to ${LINK}"
    else    
        _red "The following files have a link to the deleted file\n"
        echo ${linkfrom}
    fi
    
}

function quicknote {
    #? add quicknote [--term] :: Create a page quickly with $QUICKNOTE_FORMAT as title
    if [ -d "$INBOX" ]; then
        cd "$INBOX"
    else
        mkdir -p "$INBOX"
        cd "$INBOX"
    fi
    if [ "$1" = "--term" ]; then
        quicknoteterminal
        exit 0
    fi
    local qnfn="`date +$QUICKNOTE_FORMAT`"
    ${ZOEDITOR} "${qnfn}.md"
}

function quicknoteterminal(){
    cd ${INBOX}
    echo "Write your note and press Ctrl+d to save"
    printf "$fg[green]${(l:$COLUMNS::=:)}$reset_color\n"
    local qnfn="`date +$QUICKNOTE_FORMAT`"
    cat >  "${qnfn}.md"
}
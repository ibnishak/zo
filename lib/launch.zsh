function openpage {
    #? open page :: Launch/View page
    cd ${WIKIROOT} && ${ZOEDITOR} "$(printf "%s\n" **/*(.) | ${LAUNCHER})"
}

function openindex {
    #? open index :: Open main index of zo
    ${ZOEDITOR} ${WIKIROOT}/index.md
}

function openfolder(){
    #? open folder :: open containing folder of the file
    cd ${WIKIROOT}
    local file=$(printf "%s\n" **/*(.) | ${LAUNCHER})
    echo $file
    ${ZOOPEN} $file:A:h
}
function addtag {
    #? add tag :: Create new tags
    pushd ${TAGS} > /dev/null
    local tagtml=$(cat ${TAGTEMPLATE})
    local newtag=$1
    if [ -z "$newtag" ]; then
        vared -p "New tag: " -c newtag
    fi
    echo ${(e)tagtml} > ${newtag}.md # Write tag-template to tag-file.
    local tagindex="- [${newtag}](./tags/${newtag}.md)" # Append tag to index-file
    sed -i "$ a ${tagindex}" ${WIKIROOT}/index.md
}

function tagline_constructor {
    pushd ${TAGS} > /dev/null
    f=($(fd --type f --extension .md . -x echo {/.} | fzf --reverse --height=40% --prompt "Tags: " -m --bind=ctrl-j:print-query))

    for i in $f; do
        [ ! -f "${i}.md" ] && addtag ${i} ## If tag does not exist, add it
        TAGARR+=("[${i}](../../../tags/${i}.md)") ## Make an array of tags
        local indexline="- [${TITLE}](../${FILE})" # Add the filename to each tag file
        sed -i "$ a ${indexline}" ${WIKIROOT}/tags/${i}.md
    done

    TAGLINE=${(j:, :)TAGARR} # Join tags by comma

    for i in $f; do
        if [ ${TAGHOOKS[$i]+_} ]; then
            "${TAGHOOKS[$i]}"
        fi
    done
    popd > /dev/null
}


function addpage {
    #? add page [TITLE] :: Create a new page in today's directory with tags
    TITLE="$@" # TITLE, TAGLINE and FILE must remain global for taghooks.
    if [ -z "$TITLE" ]; then
        vared -p "Title: " -c TITLE
    fi
    # FIXME: Use checkempty and one liners
    if [ -z "$TITLE" ]; then
        echo "Empty title. Zo exiting"
        exit 1
    fi

    FILE=$(echo ${TITLE} | sed -e 's/[^A-Za-z0-9._-]/-/g').md
    FILE=$(echo ${FILE} | tr '[A-Z]' '[a-z]')
    FILE=$(date +%Y)/$(date +%m)/$(date +%d)/$FILE
    mkdir -p $(date +%Y)/$(date +%m)/$(date +%d)
    # touch $FILE
    tagline_constructor
    local pgtml=$(cat ${PAGETEMPLATE})
    echo ${(e)pgtml} > $FILE
    ${ZOEDITOR} $FILE
}

function renamepage {
    #? rename page :: Rename pages
    LINKTO="$@"
    if [ -z "$LINKTO" ]; then
        vared -p "Enter the desired title to link to:" -c LINKTO
    fi
    if [ -z "$LINKTO" ]; then
        echo "No search term provided. zo Exiting"
        exit 2
    fi
    LINK=$(fd --type f --extension md ${LINKTO} | ${LAUNCHER})
    if [ -z "$LINK" ]; then
        echo "No results found. zo Exiting"
        exit 3
    fi
    vared -p "Title: " -c TITLE
    FILE=$(echo ${TITLE} | sed -e 's/[^A-Za-z0-9._-]/-/g').md
    FILE=$(echo ${FILE} | tr '[A-Z]' '[a-z]')
    FILE=$(date +%Y)/$(date +%m)/$(date +%d)/$FILE
    mkdir -p $(date +%Y)/$(date +%m)/$(date +%d)
    mv $LINK $FILE
    ambr $LINK $FILE
}

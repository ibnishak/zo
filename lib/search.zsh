function search {
    #? search :: Search
    local search="$@"
    if [ -z "${search}" ]; then
        vared -p "Search for: " -c search
    fi
    if [ -z "${search}" ]; then
        echo "Search term not found. zo exiting"
        exit 1
    fi

    local rg_command=( "rg" "--column" "--line-number" "--no-heading" "--smart-case" "--color" "always" "--colors" "match:fg:yellow" )
    local result=$(${rg_command} ${search} | fzf --ansi --reverse)

    if [ -z "${result}" ]; then
        echo "No results found!"
    else
        local fname=$(echo $result | cut -d ":" -f1)
        local line=$(echo $result | cut -d ":" -f2)
        local col=$(echo $result | cut -d ":" -f3)

        case ${ZOEDITOR} in
            code*)
                ${ZOEDITOR} -g ${fname}:${line}:${col}
                ;;
            *vim|vi)
                ${ZOEDITOR} +${line} ${fname}
                ;;
            emacs*)
                ${ZOEDITOR} +${line}:${col} ${fname}
                ;;
            subl*)
                ${ZOEDITOR} ${fname}:${line}:${col}
                ;;
            *)
                ${ZOEDITOR} ${fname}
                ;;
        esac
    fi
}

function copytags {
    #? copy tags :: Create and Copy tagline to clipboard
    cd ${TAGS}
    local tagfiles=(${(f)"$(fd --type f --extension .md . -x echo {/.} | fzf -m --bind=ctrl-j:print-query)"})
    declare -a TAGLINE
    for i in ${tagfiles}
    do
        TAGLINE+=("[${i}](../../../tags/${i}.md) ") ## Make an array of tags
    done
    if [[ -z $TAGLINE ]]; then
        echo "No tags selected. zo Exiting"
        exit 4
    fi
    printf "${TAGLINE}"
    printf "%s" "${TAGLINE}" | ${CLIP}
    printf "\n$fg[green]Tags copied to clipboard\n"
}

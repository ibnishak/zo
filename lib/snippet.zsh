function addsnippet(){
    #? add snippet [DESCRIPTION] [--history] :: Add/Create a new snippet
    [[ $1 = "--history" ]] && snippetfromhistory
    cd ${SNIPPETS}
    desc="$@"
    [ -z "$desc" ] && vared -p "Description: " -c desc
    checkempty "$desc"
    desc="${desc//[^A-Za-z0-9._ -]/_}.md"
    touch "${desc}"
    ${ZOEDITOR} "${desc}"
}

function launchsnippet(){
    #? launch snippet :: Print/Execute snippets
    cd ${SNIPPETS}
    local file=$(printf "%s\n" **/*(.) | ${LAUNCHER})
    checkempty "$file"
    tmp=$(mktemp -t tmp.XXXXXXXXXX.md)
    # Removes tmp on exit or ctrl+c interruption
    trap "rm -f ${tmp} &>/dev/null" 0 2 3 15
    cat "${file}" >> ${tmp}
    ${ZOPAGER} "${tmp}"
    local patt=$(rg -o '<<(.*?)>>' -r '$1' ${tmp})
    local vars=("${(f)patt}")

    if ! rg -Fq 'zos' ${tmp} && [ "${#vars[@]}" -eq "0" ] ; then
        sed '/```/d' ${tmp} | ${CLIP}
        _green "Snippet copied!" && exit 0
    elif rg -q "# zos:copy" ${tmp}; then
        sed "/\(\`\`\`\|zos\)/d" ${tmp} | ${CLIP}
        _green "Snippet copied!" && exit 0
    fi

    for i in ${(u)vars}; do
        key=${i%%:*}
        value=${i#*:}
        echo "Value for <<$key>>? (Default:$value)"
        read input
        [[ ! -z $input ]] && value="${input}"
        sed -i "s/<<$i>>/$value/g" ${tmp}
    done
    if rg -q "# zos:exec" ${tmp}; then
        sed -ie '/```/d' ${tmp}
        zsh ${tmp}
    elif rg -q "# zos:print" ${tmp}; then
        sed '/zos/d' ${tmp}
    else
        sed -e '/```/d' ${tmp}  | ${CLIP}
    fi
}

function opensnippet(){
    #? open snippet :: Open snippet file in editor
    cd ${SNIPPETS}
    local file=$(printf "%s\n" **/*(.) | ${LAUNCHER})
    checkempty "${file}"
    ${ZOEDITOR} "${file}"
}

function snippetfromhistory(){
    local content=$(fc -lrr1 \
            | fzf -e -i +s --reverse --tac --margin=4%,1%,1%,2%  --prompt='Enter string to filter history > ' \
        | sed 's/ *[0-9]* *//')
    local title
    vared -p "Enter description of snippet: " -c title
    local slug="${title//[^A-Za-z0-9._ -]/_}.md"
    echo $content > $slug
    ${ZOEDITOR} "${slug}" && exit 0
}

function checkempty(){
    if [ -z "$1" ]; then
        _red "Looks like user interruption.\n"
        echo "Zo exiting"
        exit 1
    fi
}

function addurl {
    #? add url :: Download articles in markdown format
    url="$@"
    [ -z "$url" ] && vared -p "url: " -c url
    checkempty "$url"

    mkdir -p $(date +%Y)/$(date +%m)/$(date +%d)
    cd $(date +%Y)/$(date +%m)/$(date +%d)
    
    zo-scraper -add $url
    local fn=$(ls *(om[1]))
    FILE=$(date +%Y)/$(date +%m)/$(date +%d)/$fn
    TITLE=$(head -n1 $(ls *(om[1])) | sed "s/.*TITLE:\s//")
    tagline_constructor
    local t="- #+TAGS: $TAGLINE"
    sed -i "1 a $t" $fn 
    ${ZOEDITOR} $fn
}

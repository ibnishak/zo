function replacetag {
    #? replace tag :: Replace tags in files one by one
    pushd ${TAGS} > /dev/null
    a=($(fd --type f --extension .md . -x echo {/.} | fzf --reverse --height=40% --prompt "old tag: "))
    b=($(fd --type f --extension .md . -x echo {/.} | fzf --reverse --height=40% --prompt "New tag: "))
    popd > /dev/null
    ambr --regex "\[$a\]\(\.\./\.\./\.\./tags/$a\.md\)" "[$b](../../../tags/$b.md)"
}